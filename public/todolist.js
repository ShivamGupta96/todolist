$(function(){
    // console.log("Some stuff...");

    let newtodo = $('#newtodo');
    let addtodoBtn = $('#addtodo');
    let todolist = $('#todolist');
    let newTodo = newtodo.val();

    addtodoBtn.click(function(){

        $.ajax({
            type: 'POST',
            url: 'http://localhost:4567/todos',
            dataType: 'json'
        })
        .done(function(data){
            todolist.empty();
            for(var i=0;i<data.length;i++){
                $('#todolist').append("<li>" + data[i].task + "</li>");
            }
            todolist.append(newTodo);
        })
        .fail(function(jqXHR, textStatus, err){
            console.log(textStatus);
        })

    });

});

// console.log("Page is loaded...");