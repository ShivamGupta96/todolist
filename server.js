const express = require('express');
const srv = express();
const todoRoute = require('./routes/todos');

const port = 4567;

srv.use(express.json());
srv.use(express.urlencoded({extended: true}));

srv.use('/', express.static(__dirname + '/public'));

srv.use('/todos', todoRoute);

srv.listen(port, () => {
    console.log("Serving on 'http://localhost:'" + port);
});